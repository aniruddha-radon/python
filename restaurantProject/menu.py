menuList = [
    {
        "name": "Gnocchi Pesto",
        "price": 1350,
        "codeNumber": "100"
    },
    {
        "name": "Truffle Tagliatelle",
        "price": 2150,
        "codeNumber": "101"
    },
    {
        "name": "Tortellini inbrodo",
        "price": 1500,
        "codeNumber": "102"
    },
    {
        "name": "Burrata Salad",
        "price": 1350,
        "codeNumber": "103"
    },
    {
        "name": "Tiramisu",
        "price": 1200,
        "codeNumber": "104"
    }
]
