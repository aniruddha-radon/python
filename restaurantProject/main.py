import os


from menu import menuList
from utils import *

space = " "
isInvalidCodeEntered = False

def isOrderValid(order):
    return len(list(filter(lambda x: x["codeNumber"] == order, menuList))) > 0

while True:

    if isInvalidCodeEntered:
        printr("\nInvalid code number \n")

    print("\n-----MENU-----\n")

    for dish in menuList:
        printg(f"{dish['codeNumber']}  {createDisplayName(dish['name'])} {dish['price']}")

    print("\npress X to exit\n")
    order = input('\nEnter dish code to order\n')

    if(order.upper() == 'X'):
        break

    if(not isOrderValid(order)):
        isInvalidCodeEntered = True
        os.system('cls')
        continue

    isInvalidCodeEntered = False


printr("\n\nEXITING RESTAURANT MENU APPLICATION...")