RED = '\033[31m'
GREEN = '\033[32m'
END = '\033[0m'


def printr(message):
    print(f"{RED}{message}{END}")


def printg(message):
    print(f"{GREEN}{message}{END}")


def createDisplayName(name):
    return name + " " * (40 - len(name))
    
