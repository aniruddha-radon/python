# iterables
number = 78 # NOT AN ITERABLE
string = "this is a string"
#        1  2  3  4  5 --> POSITIONS
array = [11, 12, 13, 14, 15]
#        0  1  2  3  4 --> INDEX

# for num in array:
#     print(f"starting with = {num}")
#     if num % 2 == 0:
#         print(f"{num} is EVEN")
#     else:
#         print(f"{num} is ODD")
#     print(f"ending with = {num}")

markList = [89, 78, 67, 56, 45, 56, 76, 87, 98]
length = len(markList)
total = 0

for marks in markList:
    total = total + marks

print(total)
percentage = total/length
print(f"You scored {round(percentage, 2)}%")