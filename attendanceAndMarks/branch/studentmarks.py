from classes.student import Student
from classes.marks import Marks
import json
import os

PATH_TO_STUDENT_JSON = os.path.join(
    os.path.dirname(__file__), '../data/students.json')


class StudentMarks:

    @staticmethod
    def start():
        print("starting student marks")

        print("1. All Students")
        print("2. Specific Student (by roll number)")

        with open(PATH_TO_STUDENT_JSON, "r") as file:
            fileContents = file.read()
            fileContents = json.loads(fileContents)
            file.close()

        userChoice = input()

        if userChoice == "1":
            for index, student in enumerate(fileContents):
                student = json.loads(student)
                print(f"Enter Marks for roll number - {student['roll']}")
                marks = Marks()
                marksJson = json.dumps(marks.__dict__)
                student["marks"] = marksJson
                fileContents[index] = json.dumps(student)
            
            with open(PATH_TO_STUDENT_JSON, "w") as file:
                file.write(json.dumps(fileContents))
                file.close()
        elif userChoice == "2":
            roll = input("Enter Roll Number")
            for index, student in enumerate(fileContents):
                student = json.loads(student)
                if student["roll"] == roll:
                    print(f"Enter Marks for roll number - {student['roll']}")
                    marks = Marks()
                    marksJson = json.dumps(marks.__dict__)
                    student["marks"] = marksJson
                    fileContents[index] = json.dumps(student)
                    break

            with open(PATH_TO_STUDENT_JSON, "w") as file:
                file.write(json.dumps(fileContents))
                file.close()

