from classes.student import Student
import json
import os

PATH_TO_STUDENT_JSON = os.path.join(os.path.dirname(__file__), '../data/students.json')

class StudentAcceptance:

    @staticmethod
    def start():
        print("Enter student data")
        student = Student()
        studentJSON = json.dumps(student.__dict__)
        with open(PATH_TO_STUDENT_JSON, "r") as file:
            fileContents = file.read()
            fileContents = json.loads(fileContents)
            fileContents.append(studentJSON)
            file.close()

        with open(PATH_TO_STUDENT_JSON, "w") as file:
            file.write(json.dumps(fileContents))
            file.close()