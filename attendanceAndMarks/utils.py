def printMainMenu():
    print("")
    printg("Choose the module \n")
    printg("1. Enter New Student Data")
    printg("2. Enter Student Attendance")
    printg("3. Enter Student Marks")
    printg("4. Exit")

def printr(message):
    print(f"\033[91m{message}\033[0m")

def printg(message):
    print(f"\033[92m{message}\033[0m")