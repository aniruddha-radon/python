from branch.studentacceptance import StudentAcceptance
from branch.studentattendance import StudentAttendance
from branch.studentmarks import StudentMarks

from utils import printMainMenu, printg, printr

while True:
    printMainMenu()

    userChoice = input()
    userChoice = int(userChoice)

    if userChoice == 1:
        StudentAcceptance.start()
    elif userChoice == 2:
        StudentAttendance.start()
    elif userChoice == 3:
        StudentMarks.start()
    elif userChoice == 4:
        break
    else:
        printr("\nINVALID CHOICE\n")