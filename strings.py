string = "0123456789 abcdefghijklmnopqrstuyvwxyz -+=~?/,."

length = len(string)
  
print(length)


# greets the user.
name = "Aniruddha"
greeting = "Helllooooooo"

#concatenation
fullGreeting = greeting + " " + name

#interpolation
fullGreeting = f"{greeting} {name}"
#print(fullGreeting)

pattern = ";lkdfjgh;l "
fiveStars = pattern * 5
print(fiveStars)

#           012345678901234567890123456
catSpeak = "The cat says meow meow meow"
meow = catSpeak[13:20:-1]
print(meow)