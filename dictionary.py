
# persons = [
#     {
#         "name": "Aniruddha",
#         "surname": "Gohad",
#         "age": 28,
#         "address": {
#             "street": "paud rd",
#             "city": "Pune",
#             "pin": 411038
#         },
#         "occupation": "IT Guy",
#         "isMarried": True,
#         "friendList": ["Aniruddha", "Heramb", "Jay"]
#     },
#     {
#         "name": "Ambareesh",
#         "surname": "Nimkar",
#         "age": 23,
#         "address": {
#             "street": "model colony",
#             "city": "Pune",
#             "pin": 41004
#         },
#         "occupation": "Chef",
#         "isMarried": False,
#         "friendList": ["Manas", "Rahul", "Heramb"]
#     },
#     {
#         "name": "Sumant",
#         "surname": "Tulshibagwale",
#         "age": 25,
#         "address": {
#             "street": "sahakarnagar",
#             "city": "Pune",
#             "pin": 411055
#         },
#         "occupation": "Pariksha Dene",
#         "isMarried": False,
#         "friendList": ["Bot"]
#     }
# ]

# persons = []

# while True:
#     person = {}
#     name = input("Enter Name: \n")
#     surname = input("Surname sang: \n")
#     age = input("Age saang: \n")
#     occupation = input("occupation sang: \n")
#     isMarried = input("Married (Y/N): \n")

#     person["naav"] = name
#     person["surname"] = surname
#     person["age"] = age
#     person["occupation"] = occupation
#     person["isMarried"] = False if isMarried.upper() == 'N' else True #Ternary Expression

#     persons.append(person)

#     continueAddingPersons = input("Do you want to add more persons (Y/N)")
#     if(continueAddingPersons == 'N'):
#         break


# print(persons)



building = {
    "floors": 11,
    "society": "Kanchan Vastu",
    "wing": "A"
}

#print(building["society"])

building["street"] = "Paud Road"

# building = {
#     "floors": 11,
#     "society": "Kanchan Vastu",
#     "wing": "A",
#     "street": "Paud Road"
# }

#print(building)

buildings = [
    {
        "name": "Rachana Rainbow",
        "floors": 5,
        "wing": "A"
    },
    {
        "name": "Saraswati Niwas",
        "floors": 3,
        "wing": "A"
    }
]

for building in buildings:
    print(building["name"])
