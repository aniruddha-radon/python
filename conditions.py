age = 16
legalDrinkingAge = 25

isEligible = age != legalDrinkingAge
# conditional operators
# >, <, >=, <=, ==, !=

# if age >= legalDrinkingAge:
#     print("Ye le daaaru")
# elif age >= 21:
#     print("Beeeeeeeeeerrrrr")
# elif age > 15:
#     print("Ghe gutka")
# else:
#     print("Chal haaaaatttt")

sub1 = 67
sub2 = 78
sub3 = 83

percentage = (sub1 + sub2 + sub3)/3

today = "Friday"
time = 13
#Logical Operators
if today == "Friday" and time > 12 and 11 > 9:
    print("Weekend has started")

order = "pizza"
if order == "samosa" or order == "upma" or order == "sada dosa":
    print("Fire Ambareesh")