# DICTIONARY / MAP / OBJECT

# student = ["Aniruddha", "Shirish", "Gohad", "Apte Road", "12th"]

student = {
    "name": "Aniruddha",
    "middleName": "Shirish",
    "age": 28,
    "occupation": "IT",
    "surname": "Gohad"
}
surname = student["surname"]
