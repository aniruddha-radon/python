import random
import json
import requests
# javascript object notation
# JS         O      N
# data format

# dictionary or array of dictionaries

# person = {
#     'name': 'Heramb',
#     'surname': 'Joshi'
# }

# personJson = json.dumps(persons) # convert python datatype to json

# personDict = json.load({
# 	"name": "Heramb",
# 	"surname": "Joshi"
# })


baseURL = 'https://post-user-data.firebaseio.com/'
table = 'user.json'

#response = requests.get(f'{baseURL}{table}')
#data = response.json()
# person = {
#     'name': 'Ambareesh',
#     'surname': 'Nimkar'
# }

# requests.post(f'{baseURL}{table}', data=json.dumps(person))


response = requests.get('https://jsonplaceholder.typicode.com/posts')
data = response.json()

for post in data:
    print(post['body'])
