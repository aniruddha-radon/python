# datatypes ->
# numbers (int, decimals => float)
# strings
# booleans (True or False)

# [] => List/Array
dailyPracticeHours = [4, 5, 7, 2, 3, 5, 1, 6, 3, 2, 7, 4, 2, 5.3]
print(f"{dailyPracticeHours[4]} hrs of practice was done on the 3rd day")

myFriends = ["Aniruddha", "Aniruddha", "Prakash", "Om"]
print(myFriends)

studentInformation = ["Lenovo", "Seremban, Malaysia", 20, True, 29, [25, 88, 76]]

print(f"Is Married: {studentInformation[3]}")