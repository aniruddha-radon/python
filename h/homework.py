def getMax(arr):
    highest = 0
    for num in arr:
        if num > highest:
            highest = num
    return highest


def getSecondHighest(arr):
    highest = 0
    secondHighest = 0
    for num in arr:
        if num > highest:
            secondHighest = highest
            highest = num
        elif num > secondHighest and num != highest:
            secondHighest = num
    return secondHighest

numbers = [33, 4, 10, 12, 14, 44, 55, 76, 65, 1]
#         [33, 65, 76, 55, 44, 14, 12, 10, 4, 1]

def reverseArray(arr):
    length = len(arr) - 1
    for index in range(0, int((length + 1) / 2)):
        currentNumber = arr[index]
        numberToSwap = arr[length - index]
        arr[index] = numberToSwap
        arr[length - index] = currentNumber
    return arr

result = reverseArray(numbers)
#print(result)


# PALLINDROME

# O(1) => BEST CASE
# O(n) => 2nd BEST CASE
# O(2n) => MEDIUM BEST CASE
# O(n^2) => GHANERDI CASE
# 

# def isPalindrome(word):
#     char2 = [] # 1
#     char3 = "" # 1
#     for index in range(1, len(word) + 1):
#         char2.append(word[-index]) # n
#     for char in char2:
#         char3 += char # n
#     if char3 == word: # 1
#         print("is Pallindrome")
#     else:
#         print("not a pallindrome") # 1
#     return char3 # 1

# char4 = isPalindrome(userInput)
# print(char4)

def isPallindrome(string):
    return string == string[::-1]

isPallindrome = lambda x: x == x[::-1]


def getNumberOfWords(string):
    wordCount = 0
    string = string.strip()
    for index, char in enumerate(string):
        if char == " " and string[index - 1] != " ":
            wordCount += 1
        #   wordCount = wordCount + 1
    return wordCount + 1

result = getNumberOfWords("My name is Aniruddha")

numbers = [5, 123, 13, 5, 32, 673, 98, 5]
def getFirstIndex(arr, search):
    for index, elem in enumerate(arr):
        if elem == search:
            return index
    return -1

result = getFirstIndex(numbers, 15)

def getLastIndex(arr, search):
    index = len(arr) - 1
    for el in arr:
        if arr[index] == search:
            return index
        index -= 1
    return -1

result = getLastIndex(numbers, 5)
# print(result)

# string = ameehs
# search = sh

def checkSubString(string, search):
    isPresent = False
    for index, char in enumerate(string):
        if index == len(string) - 1:
            break
        if char.lower() == 's' and string[index + 1].lower() == 'h':
            isPresent = True
            break
    return isPresent

result = checkSubString("haridas", "sh")
# if result:
#     print("Tu amit shah")
# else:
#     print("Tu rahul gandhi")

# haridas
def checkName(name):
    name = name.lower()
    for index, char in enumerate(name):
        if char == "h" and name[index - 1] == "s" and (index -1) >= 0 :
            return "Mota Bhai"
    return "pappu saala"

# result = checkName(userInput)
# print(result)

# *   = 0
# * * = 1


# def printPattern(depth):
#     for rowNumber in range(depth):
#         print("*" * (rowNumber + 1))

# printPattern(5)

# def printPattern(depth):
#     for rowNumber in range(depth):
#         print(f'{(depth - (rowNumber + 1)) * "  "}{(rowNumber + 1) * " *"}')

# printPattern(5)


def calculateCost(numOfPeople, complexity, hours):
    baseCost = 1500
    if complexity == 'H':
        baseCost += 200
    elif complexity == 'M':
        baseCost += 150
    else:
        baseCost += 100
    totalPrice = baseCost * hours
    return totalPrice

quote = calculateCost(4, 'H', 25)
# print(f'Total Rs.{quote} is required for completing the job')
#'^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

def checkEmail(email):
    # email = aniruddha@gmail.com.
    alphabets = 'abcdefghijklmnopqrstuvwxyz'
    emailArray = email.split('@')
    # ['aniruddha', 'gmail.com']
    if len(emailArray) < 2:
        return False
    elif len(emailArray[0]) < 1 or emailArray[0][0] not in alphabets or len(emailArray) > 2:
        return False

    nextPart = emailArray[1]
    if "." not in nextPart or nextPart[0] == "." or nextPart[len(nextPart) - 1] == ".":
        return False
    
    return True

result = checkEmail('aniruddha@gmail.com')

def getMajorScale(root):
    notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']
    pattern = [2, 2, 1, 2, 2, 2, 1]
    currentIndex = notes.index(root)
    scale = ''
    for i in pattern:
        scale += f' {notes[currentIndex]}'
        currentIndex = (currentIndex + i) % len(notes)
    return scale

#print(getMajorScale('D'))


randomArray = [12, 4, 23, 42, 6, 18]

def sortArray(arr):
    length = len(arr)
    for i in range(length):
        for j in range(i+1, length):
            if arr[i] > arr[j]:
                temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
    return arr

# result = sortArray(randomArray)
# print(result)
#                                               
sortedArray = [3, 4, 8, 11, 12, 14, 16, 19, 24, 31, 33, 34, 40, 42, 47, 48, 50, 51, 52]
def findIndexInSortedArray(arr, number):
    length = len(arr)
    leftIndex = 0
    rightIndex = length - 1
    midIndex = int(length/2)
    while True:
        if(arr[midIndex] == number):
            return midIndex
        
        if arr[midIndex] > number:
            rightIndex = midIndex
            midIndex = int((midIndex - leftIndex) / 2)
        
        if arr[midIndex] < number:
            leftIndex = midIndex
            midIndex = int((rightIndex + midIndex) /2)


# result = findIndexInSortedArray(sortedArray, 52)
# actual = sortedArray.index(52)

# if(result == actual):
#     print('NICE')
# else:
#     print('BOCHA')

# HANGMAN

word = {
    'word': 'circle',
    'hint': 'a round thing.'
}

def hangman(word):
    # word => bat
    tries = len(word) + 2
    dashedWord = "_ " * len(word)
    
    print (dashedWord)

    while True:
        letter = input("Enter letter: ")
        # t
        for index, x in enumerate(word):
            # _ _ _ 
            # b a t

            # a = 'bat'
            # a[:2] => ba
            # a[2:] => t
            if x == letter:
                preSubString = dashedWord[:(index * 2)] # _ _ 
                postSubString = dashedWord[(index * 2) + 1:] # 
                dashedWord = f'{preSubString}{letter}{postSubString}' # _ _ t 

        print (dashedWord)

        tries -= 1
        if tries == 0 and dashedWord.count("_ ") != 0:
            return False

        if dashedWord.count("_ ") == 0:
            return True

# print(word['hint'])
# check = hangman(word['word'])
# if check:
#     print("Won!!!")
# else:
#     print("Lost!!!")

import random

def generatePassword(length):
    characters = 'abcdefghijklmnopqrstuvxyz123456789'
    specialChars = '!@#$%^&*()_+'
    capitalAt = random.randint(1, length - 2)
    specialCharAt = random.randint(1, length - 2)
    if specialCharAt == capitalAt:
        specialCharAt += 1
    password = ''
    for i in range(length):
        if capitalAt == i:
            password += characters[random.randint(0, 25)].upper()
            continue

        if specialCharAt == i:
            password += specialChars[random.randint(0, 11)]
            continue

        password += random.choice(characters)

    return password

# print(generatePassword(8))
random.randint

def rpsGame():
    userInput = input('Enter rock, paper, scissor')
    choices = ['rock', 'paper', 'scissor']
    computerChoice = random.choice(choices)
    print(computerChoice)
    if userInput not in choices:
        print('Invalid input')
        return

    if computerChoice == userInput:
        print('Its a tie!')
        return
    
    if (computerChoice == 'rock' and userInput == 'paper') \
    or (computerChoice == 'paper' and userInput == 'scissor') \
    or (computerChoice == 'scissor' and userInput == 'rock'):
        print('You WON!')
    else:
        print('You Lost!!')


# rpsGame()

# numbers = [12, 14, 15, 17, 17, 17, 25, 31, 32]
# numberCount = {}
# max_number = [0, 0]
# for number in numbers:
#     if number in numberCount:
#         numberCount[number] += 1
#         if numberCount[number] > max_number[0]:
#             max_number = [numberCount[number], number]
#     else:
#         numberCount[number] = 1

# print(numberCount, max_number)

matrix = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

def createShip():
    choices = ['horizontal', 'vertical']
    choice = random.choice(choices)
    rowIndex = random.randint(0, 3)
    columnIndex = random.randint(0, 3)
    for i in range(6):
        matrix[rowIndex][columnIndex] = '#'
        if choice == 'horizontal':
            columnIndex += 1
        else:
            rowIndex += 1


def printBattleGround():
    for row in matrix:
        for col in row:
            print(col, end=" ")
        print(end="\n")


def startGame():
    createShip()
    printBattleGround()
    chanceCount = 10
    boomStore = []
    while True:
        userChoice = input('enter two numbers: ')
        userCoords = userChoice.split(" ")
        x = int(userCoords[0])
        y = int(userCoords[1])
        if matrix[x][y] == '#' and (x * 10 + y) not in boomStore:
            boomStore.append((x * 10 + y))
            print('BOOOOOOM')
        else:
            print('SPLAAAASHH!')

        chanceCount -= 1
        if len(boomStore) == 6:
            print('YOU WON!')
            break

        if chanceCount == 0:
            print('You Lost!')
            break 


# startGame()
import requests
import json
getQuoteURL = 'https://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en'
def printQuote():
    response = requests.get(getQuoteURL)
    quoteDict = response.json()
    print(quoteDict['quoteText'])

# printQuote()


firebaseDataURL = 'https://post-user-data.firebaseio.com/user.json'
firebaseFileURL = 'gs://post-user-data.appspot.com/'

user = {
    'name': 'Aniruddha',
    'surname': 'Gohad'
}

# requests.post(firebaseDataURL, data=json.dumps(user))
# response = requests.get(firebaseDataURL)
# data = response.json()

# for key in data:
#     userData = data[key]
#     print(userData)
