# class => sacha/mould/blueprint
import json

class Address:
    # street, city, state

    # constructor
    def __init__(self, street, city, state):
        self.street = street
        self.city = city
        self.state = state

    def getFullAddress(self):
        return f'{self.street}, {self.city}, {self.state}'

    def json(self):
        return json.dumps(self.__dict__)