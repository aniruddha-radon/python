# # for 

# #for number in range(10):
# #    print(f"{number} Aniruddha")

# dailyPracticeHours = [3, 4, 7, 9, 12, 11, 13, 8, 5, 4]
# length = len(dailyPracticeHours)
# total = 0

# # for index in range(length):
# #     total = total + dailyPracticeHours[index]

# # for hour in dailyPracticeHours:
# #     total = total + hour

# # average = total / length

# # if average > 5:
# #     print("AWESOME")
# # else:
# #     print("practice more")


# # userInput = input("Enter a number")
# # userInput = int(userInput)

# # upperBound = userInput + 15

# # print(userInput)

# # print(upperBound)

# # for number in range(0, 31):
# #     print(number)



# myMarks = [12, 13, 14, 15, 16, 18, 20, 25, 30]
# length = len(myMarks)
# total = 0

# for mark in myMarks:
#     total = total + mark

# total = 0
# for index in range(0, length):
#     total = total + myMarks[index]

# print(total)


# break & continue

for number in range(15, 36):
    if number == 26:
        break
    print(number)

print("After for loop")

for number in range(5, 11):
    if number == 7:
        continue
    print(number)