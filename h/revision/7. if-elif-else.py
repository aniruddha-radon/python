# TRUTHY & FALSY values.

# '' => FALSY
# 0  => FALSY
# [] => FALSY
# () => FALSY
# {} => FALSY
# None => FALSY
# everything else is evaluated as Truthy.

# taking a decision based on the truthiness of a value or a condition.
# we use if, elif and else.

# if <some-condition is true> or <truthiness>:
#    run this block

# if condition/truthiness of if block fails.
# elif <some-condition is true> or <truthiness>:
#    run this block

# if no condition is satisfied in either if or elif
# else:
#    run this block

number = 0

if 7 > 3 and 6 < 1:
    number = 4
elif 6 == 7:
    number = 1
else:
    number = -1

number = 0

if number and '' or [] or 'hello':
    print('hi')
else:
    print('bye')


if 'a':
    print('a')

# if does not compulsorily require elif and else.
# it is okay to omit them.

# elif or else require if to be present and cannot be present standalone.