num = 10
num2 = 15

# addition
num + num2 # + operator to add 2 numbers.
addition = num + num2 # stores the addition in variable 'addition'

addition = 10 + 15 + num + 20 + num2
# valid addition. we can add multiple numbers using + operator.

# subtraction
num2 - num # - operator to subtract 2 numbers.
subtraction = num2 - num # stores the subtraction in variable 'subtraction'

subtraction = 15 - 10 - num - 20 - num2

# multiplication
num * num2 # * operator to multiply 2 numbers.
multiplication = num * num2 # stores the multiplication in variable 'multiplication'

# division
num2 / num # / operator to divide the numbers.
division = num2 / num # result is float.

# UTILITY OPERATORS.

divisionResultInInteger = 5 // 3 # // operator to get result of division in int.
remainder = 10 % 4 # % => MOD. returns the remainder of the division 10/4
# remainder = 2
exponent = 5 ** 3 # ** to raise a number to an indice.
# exponent = 125


# SHORTCUTS FOR ASSIGNMENT.

counter = 0
counter = counter + 1
counter += 1 # add 1 to counter.
counter -= 1 # subtract 1 from counter
counter /= 5 # divides counter by 5.
counter *= 6 # multiplies counter by 6.