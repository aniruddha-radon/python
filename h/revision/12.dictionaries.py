# What are dictionaries?
# comma separated list of key:value pairs, enclosed by { }
# values can contain all datatypes.
# generally keys are strings or numbers
# key and value is separated by a :
# not index based.

myDictionary = {
    'make': 'Lenovo',
    'yearsOwned': 3,
    'isWorking': True,
    'usageByDay': [7, 8, 9, 6, 7, 8, 6, 9],
    'usageStatisticsByDay': [
        {
            'CPUUsage': 97,
            'consumption': 6.7,
            'ramUsed': 45
        },
        {
            'CPUUsage': 96,
            'consumption': 7.7,
            'ramUsed': 78
        }
    ],
    'ownerDetails': {
        'name': 'Heramb',
        'address': 'Gautam Apts',
        'contact': '0123456789'
    },
    0: 'first index'
}

myDictionary['yearsOwned'] # accesses the value at key yearsOwned
# if key is not present, throws an error.

myDictionary['yearsOwned'] = 17
# updates the value of the key yearsOwned
# if key is not present, creates the key and assigns the value.

myDictionary.get('yearsOwned')
# accesses the value at key yearsOwned
# BUT if key is not present, will not throw error but will return None.

myDictionary.keys() # returns a set(datatype) of all the keys.
myDictionary.values() # returns an iterator of values.

del myDictionary['yearsOwned'] # deletes the key value pair.


# nested elements access.
myDictionary['usageStatisticsByDay'][0]['ramUsed']