# what are variables?

x = 5
# x is 5. x is equal to 5. x madhe 5 store zala ahe.

x = 10
# x was overwritten by 10. no longer holds the value 5. 
# = is assignment operator.
# = assigns the RHS to the variable(LHS)

name = 'Heramb'
# name was assigned the value 'Heramb'

x = name
# x madhe name chi value store hotie.
# mhanun x = 'Heramb'