# what is datatype?
# jo data store hotoy tyacha type.
# data cha type.

# types of data (datatypes)

integer = 12 # data => 12, type of 12 => int.
# that is why integer is of type int.

floating = 17.5 # data => 17.5, type of 17.5 => float
# that is why floating is of type float.

string = '1234567890abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+=-:".?/,<>|\`~*-+'
# data => '1234567890abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+=-:".?/,<>|\`~*-+'
# type => str.
# that is why string is of the type str.

boolean = True # data => True, type of True => bool
# booleans can only be of 2 types -> True or False.
# that is why boolean is of type bool.

iDontHaveAValue = None # data => NOTHING. type of None => None
# that is why iDontHaveAValue is of type None.

# main types => Number (int, float), string, boolean, None.
# PRIMITIVES.

# COLLECTION of PRIMITIVES.
# List, Tuple, Dictionary

myList = [12, 17.5, 'ek string', False, None]
# comma separated elements (of any datatype) enclosed with square brackets
# is called a list (array)

myTuple = (12, 17.5, 'ek string', False, None)
# comma separated elements (of any datatype) enclosed with round brackets
# is called a tuple

# Difference between Tuple and List.
# Tuple is IMMUTABLE => cannot change the value directly.
# List is MUTABLE => can change value directly
myList[0] = 13
# [13, 17.5, 'ek string', False, None]

myTuple[0] = 14
# cannot do this.

myTuple = (14, 17.5, 'ek string', False, None)
# this will work.

# list can have other lists as elements.
# this forms a 2D list.

twoDList = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

# twoDList has 3 elements.
# to access 1
twoDList[0] # element at index 0 => [1, 2, 3]
twoDList[0][0] # element at index 0 => [1, 2, 3], and element at index 0 of this list => 1
# to access 6
twoDList[1][2]

# DICTIONARY

myDictionary = {
    'name': 'Heramb', # key:value 
    'surname': 'Joshi' # key:value
}
# dictionary hold multiple key:value pairs separated by comma
# and is enclosed by curly brackets

# access a value of a particular key =>
# nameOfDictionary[nameOfKey] 
# fetches the value of the key inside that dictionary.

myDictionary['surname'] # will fetch the value 'Joshi'
myDictionary['surname'] = 'Gaikwad' # will change the value of the key to 'Gaikwad'

# nested dictionaries

myDictionary = {
    'name': 'Heramb',
    'address': { # address is a nested dictionary.
        'street': 'Karve Road',
        'city': 'Pune',
        'building': {
            'name': 'Gautam',
            'builder': 'chongu'
        }
    }
}

myDictionary['address'] # will fetch the dictionary  attached to the key address
# {
#    'street': 'Karve Road',
#    'city': 'Pune'
# }

myDictionary['address']['city'] # will fetch the value attached to the key 'city'
# inside the dictionary attached to the key 'address'

# to access the value 'chongu'
myDictionary['address']['building']['builder']