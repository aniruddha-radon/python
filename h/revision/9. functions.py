# reusable named block of code, which can accept some parameters(data)
# and can return some data.

# def => tells python to create a function
# def - COMPULSORY <name of function - COMPULSORY>(<comma seperated parameters - OPTIONAL>):
#     code inside the function 
#     1 line of code is - COMPULSORY
#     return <some-data> - OPTIONAL
#     if no return, then python returns None.

# def does not execute the function.
# functions are executed only when they are called.

# non parameterized function without return.

# printHelloWorld() - will not work because function is called before it is defined.

def printHelloWorld():
    print('Hello, World!')

printHelloWorld() # invokes the function. starts executing the function.
# <name of function>()

# parameterized function without return
def add(num1, num2):
    print(num1 + num2)

add(5, 7) # this invokes the function add with parameter values as 5 and 7
# <name of function>(param1Value, param2Value, ......, paramNValue)

# non parameterized function with return
def getPIValue():
    return 3.142 # returns the value to where it was called from.

PI = getPIValue() # this invokes the function.
# stores the value returned from getPIValue in PI.

Pi2 = getPIValue()
pI3 = getPIValue()

# parameterized function with return

def getFullName(name, surname):
    fullName = f'{name} {surname}'
    return fullName

aniruddhaFN = getFullName('aniruddha', 'gohad')
herambFN = getFullName('heramb', 'joshi')


# lambda functions
# lambda functions dont have names.
# lambda functions are single line functions.
# lambda returns the result without return statement.
# can be stored in a variable.

#           lambda function
addLambda = lambda x: x + x
# variable in which lambda function is stored
addition = addLambda(5)

power = lambda base, indice: base ** indice

exponent = power(2, 3)