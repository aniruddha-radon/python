# map

listOfPrimeNums = [2, 3, 5, 7, 11, 13, 17, 19]
# get a new list of squares of all the numbers in listOfPrimeNums

squaresOfPrime = []

for number in listOfPrimeNums:
    square = number ** 2
    squaresOfPrime.append(square)

squaresOfAllPrimes = map(lambda n: n ** 2, listOfPrimeNums)

myMap = lambda fn, sequence: [fn(n) for n in sequence]
    # newSequence = []
    # for n in sequence:
    #     result = fn(n)
    #     newSequence.append(result)
    # return newSequence

print(myMap(lambda x: x ** 2, listOfPrimeNums))

# filter 
listOfRandomNumbers = [12, 14, 15, 16 , 21, 22 ,33, 34, 44]
# get a NEW list with a filter of odd numbers.

listOfOddNumber = filter(lambda x: x % 2 != 0, listOfRandomNumbers)
# function in filter should always return a boolean
myFilter = lambda fn, s: [fn(x) for x in s]

# REDUCE
from functools import reduce
# cumulative total
reduce(lambda cumulativeTotal, currentValue: cumulativeTotal + currentValue, listOfOddNumber)