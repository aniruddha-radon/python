# a loop can be executed only and only on iterables.
# iterables are those datatypes which can be iterated upon.
# they have either indexes or keys or 'next' function.

# datatypes that can be looped on
# list, tuple, string, dictionary

# iterator functions => which return an iterator.
# range, enumerator

listOfNumbers = [10, 11, 12, 13, 14, 15]
tupleOfNumbers = (10, 11, 12, 13, 14, 15)
string = '101112131415'
dictionary = {
    '10': 10,
    '11': 11,
    '12': 12,
    '13': 13,
    '14': 14,
    '15': 15
}

# for loops

# list
for element in listOfNumbers:
    print(element)
# element is going to be the value inside the list at each index.

for index, element in enumerate(listOfNumbers):
    print(index)
    print(element)

# tuple
for element in tupleOfNumbers:
    print(element)
# element is going to be the value inside the tuple at each index.

for index, element in enumerate(tupleOfNumbers):
    print(index)
    print(element)

# string
for char in string:
    print(char)
# char is going to be the value inside the string at each index.

for index, char in enumerate(string):
    print(index)
    print(char)

# dictionary
for key in dictionary:
    print(key) # prints key
    print(dictionary[key]) # prints value

for key, value in dictionary.items():
    print(key)
    print(value)


# range(start, exclusiveEnd, step)
for number in range(10, 21, 2):
    print(number)


# WHILE LOOP
# repeat an action till some condition is met(True).
# while <some-condtion> or <truthiness>:
#       do this.

counter = 0

while counter < 10:
    print(counter)
    counter += 1

while True:
    print('me veda ahe')
# infinite loop. # aai function.

# prematurely break out of loops (for/while)
# break

# for
for number in range(100):
    print(number)
    if number > 50:
        break # stops the execution of for loop.

while counter < 100:
    print(counter)
    if counter > 50:
        break # stops the the execution of while loop

# skip one iteration of the loop
# continue

for number in range(51):
    if number % 5 == 0:
        continue # khalcha sagala skip, continue to next iteration.
    print(number)
    square = number ** 2
    print(square)

while counter < 5000:
    if counter // 2 == 4:
        continue
    print(counter)

userInput = ''
while True:
    userInput = input('kahitari lihi')
    break

# go to line 99 and start executing => NOT POSSIBLE