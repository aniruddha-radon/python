# Lists are a collection of other datatypes
# each element is separated by commas
# elements are enclosed in square brackets
# mutable.

listOfNumbers = [1, 2, 3, 4, 5, 'Six']
# INDEX          0  1  2  3  4   5

listOfNumbers[0] # accesses element at index 0
listOfNumbers[6] # Error, index 6 does not exist on list.

# difference between list and array.

# Array => same as list, except, must contain elements of same datatype.

# accessing/slicing
# nameOfList[index] # accesses element at that index

listOfNumbers[1]
listOfNumbers[:4] # accesses all elements till index 3
listOfNumbers[4:] # accesses all elements from index 4
listOfNumbers[1:5] # accesses elements from indexes 1 to 4.
listOfNumbers[-2] # accesses element at second last index
listOfNumbers[1:4:-1] # from 1 to 3 in reverse order.
listOfNumbers[::-1] # reverses the array

# to copy the values of array but not reference 
aniruddhaFriendList = ['Heramb', 'Aniruddha', 'Jay']
ameyasFriendList = aniruddhaFriendList[::]

aniruddhaFriendList.append('Meghana')

print(ameyasFriendList)

# COMMON FUNCTIONS
len(listOfNumbers) # returns the length of list
listOfNumbers.append(14) # adds 14 to the end of the list
listOfNumbers.pop() # removes the last element
listOfNumbers.pop(3) # removes the element at index 3
listOfNumbers.remove(3) # removes the element which has value 3
listOfNumbers.insert(3, 3) # adds value(element) 3 at index 3
listOfNumbers.sort() # sorts the array in ascending order
listOfNumbers.reverse() # reverses the array