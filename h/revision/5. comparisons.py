# COMPARISON OPERATORS
# compare LHS with RHS and return a boolean.(TRUE/FALSE)

# equality ==
5 == 4 # is five equal to four? No => False.
4 == 4 # is 4 equal to 4? Yes => True.

isEqual = 9 == 9 # is 9 equal to 9? Yes => True. isEqual will store True.
isEqual = 7 == 9 # is 7 equal to 9? No => False. isEqual will store False.

# not equal operator.
isNotEqual = 7 != 9 # is 7 not equal to 9? Yes => True.
isNotEqual = 9 != 9 # is 9 not equal to 9? No => False.

# >, >=
isGreater = 6 > 3 # is 6 > 3? Yes => True.
isGreaterThanEqualTo = 6 >= 6 # is 6 >= 6? Yes => True.

# <, <=
isLessThan = 3 < 6 # is 3 < 6? Yes => True.
isLessThanEqualTo = 3 <= 3 # is 3 <= 3? Yes => True.

