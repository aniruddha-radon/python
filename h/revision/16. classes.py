# what is a class?
# Dwellings -> building, bunglow, mahal, flat, society, cave
# properties of a dwelling -> 
# noOfRooms, noOfTerraces, noOfBathrooms, surfaceArea, sellableArea
# parkingArea, floor 

# Car
# type, HP, CC, powerWindows, powerSteering, geared, noOfSpeeds, 
# airbags
# start(), move(), accelerate(), break() => functionality

# class is blueprint/mould which has some properties and some methods.
# each object belonging to that class will have the properties and methods.
# an object is the instance/real life object created using that class.

# class => tells python to create a class
# class <classname>(): => creates a class with a name 

class Car():
    #__init__ => is the method which creates the object/instance
    def __init__(self, naav, prakar):
        # self refers to the object that is being created.
        self.name = naav
        self.type = prakar
        # to the object which being created, the property name will
        # be assigned the value passed as parameter naav.


swift = Car('swift dezire', 'automatic')
# swift is the object that is being created.
# Car(params) => calls the __init__ method.
# python will pass self automatically.
# 'swift dezire' will be passed as naav.
swift.name
swift.type

nano = Car('Nano', 'sedan')
nano.name
nano.type





class User():
    def __init__(self, naav, password, email):
        self.name = naav
        self.password = password
        self.email = email

    def login(self, password, email):
        if self.password == password and self.email == email:
            return True
        return False

    def changePassword(self, newPassword):
        if self.password != newPassword:
            self.password = newPassword
            return True
        return False

    def getName(self):
        return self._dummyPrivateMethod()

    def _dummyPrivateMethod(self):
        return self.name.upper()



aniruddha = User('Aniruddha', 'abcd123', 'abc@gmail.com')
aniruddha.login('abcde123', 'abc@gmail.com')
aniruddha.changePassword('pqrs!234')

# OOP
# Object Oriented Programming.

# Procedural Programming => top to bottom program, with emphasis on functions.
# OOP => top to bottom program with emphasis on data.

# Abstraction, Encapsulation, Inheritance, Polymorphism.
# Abstraction =>

from random import Random
Random.choice([1, 2, 3, 4, 5])

# we know how to use the method, but the underlying code/mechanism is hidden from us
# that is Abstraction.

# Encapsulation
# pseudo private method/properties which should be used only inside the class scope.


# __methodname__ # DUNDER methods, which special methods on an object
