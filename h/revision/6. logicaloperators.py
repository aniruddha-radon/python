# combine two or more boolean values.
# combining goes from left to right.

# OR and AND => logical operators.

#==========================================================
# VALUE1    |    VALUE2     |    OPERATOR    |    RESULT ==
#==========================================================
# TRUE      |    TRUE       |    OR          |    TRUE
result = True or True
# TRUE      |    FALSE      |    OR          |    TRUE
result = True or False
# FALSE     |    TRUE       |    OR          |    TRUE
result = False or True
# FALSE     |    FALSE      |    OR          |    FALSE
result = False or False

# TRUE      |    TRUE       |    AND         |    TRUE
result = True and True
# TRUE      |    FALSE      |    AND         |    FALSE
result = True and False
# FALSE     |    FALSE      |    AND         |    FALSE
result = False and False
# FALSE     |    TRUE       |    AND         |    FALSE
result = False and True

# OR gives TRUE when atleast one value is True.
# AND gives TRUE when both values are True.

result = (5 > 3) or (4 < 2)
#        TRUE    or FALSE  = TRUE

result = (5 > 3) or (4 < 2) or (8 != 9)
#        TRUE   or   FALSE      TRUE
#              TRUE    or  TRUE
#                     TRUE

result = (5 > 3) and (4 < 2)
#        TRUE    and FALSE  = FALSE

result = (5 > 3) and (4 < 2) and (8 != 9)
#        TRUE   and   FALSE      TRUE
#              FALSE   and  TRUE
#                     FALSE

result = (5 > 3) and (4 < 2) or (8 != 9)
#         TRUE   and   FALSE   or  TRUE
#              FALSE   or  TRUE
#                     TRUE

result = (5 > 3) or (4 < 2) and (8 != 9)
#        TRUE   or   FALSE   and  TRUE
#              TRUE   and  TRUE
#                     TRUE