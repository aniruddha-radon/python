# strings are enclosed in either single quotes or double quotes.
# string can any character from a-z, A-Z, 0-9, all special characters
# special chars => `~!@#$%^&*()_-+={}[]|\;:'"<>,.?/ and space.

name = 'Aniruddha'
char = 'P'
sentence = 'I am a sentence, formed out of words'
sequence = '0123456789'

# ESCAPE CHARACTERS.
proverb = 'it\'s raining outside' 
# use \ to tell python to ignore ' inside a single quoted string
proverb = "it's raining outside"
# alternate way to ignore ' inside a string

proverb = "it's \"my\" way of doing things."
proverb = 'it\'s "my" way of doing things.'

# SPECIAL ESCAPE CHARS => 

newline = "this is the first line.\nThis is the second line."
# \n to represent a new line.
# \t to represent a tab.

# MERGING OF STRINGS (CONCATENATION).

name = 'Aniruddha'
surname = 'Gohad'
fullName = name + ' ' + surname 
# to concatenate strings we can use the + operator.
# fullName => Aniruddha Gohad

fullName = f'{name} {surname} is my name'
# f creates a formatted string.
# to insert variable values in the string
# we use {} and inside {} we write the variable name
# {variable}

# common functions on strings

len(fullName) # gives the length of that string. 
place = '     Pune is the best    '
place.strip() # removes all the spaces from left and right.

friends = 'Aniruddha, Heramb, Jay, Ameya'
colors = 'orange | red | black | purple'

friendList = friends.split(',') # splits a string and creates an list/array.
colorList = colors.split('|')

sentence = 'hello, I am having a glass of water'
sentence.upper() # all characters are capitalized.
sentence.lower() # all characters are lowerized.
sentence.capitalize() # first letter is capital.
sentence.find('i') # returns the index of the first 'i'  otherwise return -1.
sentence.count('l') # returns the count of 'l' in the string.

sentence.index(4) # returns the character at index 4. 

# SUBSTRINGS
# INDEX    0123456789

# [StartIndex:EndIndex:Step]

company = 'Radon Tech'
company[2] # access a character at index 2.
company[2:] # access the substring from 2 till end.
company[2:7] # access the substring from index 2 to 7.
company[:5] # start to index 5
company[::2] # skips 2nd character after current character. # STEP.
company[::-1] # accesses each character in reverse order.