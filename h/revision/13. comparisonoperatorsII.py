# in, is, not in

# is
# checks if a variable is of a certain type
# checks if a variable and another variable have the same reference
# (are pointing at the same memory location)

number = 10
if type(number) is int:
    print('number is an integer')

list1 = [1, 2, 3, 4]
list2 = list1 # list2 refers to the same memory location as list1
list3 = list1[::] # copies the values and creates a new list.

if list1 is list2:
    print('both are pointing to the same memory location')

if list3 is not list1:
    print('both are NOT pointing to the same memory location')

# in
# searches a value/key inside a collection(list, tuple, dict)
listOfNumbers = [10, 15, 20, 25, 30, 35, 40]

if 30 in listOfNumbers:
    print('30 is present')

if 5 in range(1, 20):
    print('5 is in the range 1 to 19')

myDict = {
    'name': 'abcd',
    'age': 25
}

if 'name' in myDict:
    print(myDict['name'])
else:
    print('the key = name is not present')

if 'surname' in myDict:
    print(myDict['surname'])
else:
    print('the key = surname is not present')

if 'surname' not in myDict:
    print('the key = surname is not present')
else:
    print(myDict['surname'])
    