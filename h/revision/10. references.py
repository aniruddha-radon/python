# what are references?
# references are pointers to the memory.

numbers = [1, 2, 3, 4, 5]
# numbers array/list is stored at location 'af59d2'
# 'af59d2' => [1, 2, 3, 4, 5]
# numbers = 'af59d2'

numbers2 = numbers
# only the address/reference is copied into numbers2
# both numbers and numbers2 are pointing at the same memory location

# reference based datatypes =>
# list, tuple, dictionary, function


number = 10
num2 = number
# reference is not copied. The value is copied.


# ============================================= #

aniruddhaFriendList = ['Heramb', 'Aniruddha', 'Jay']
ameyasFriendList = aniruddhaFriendList
# ameyasFriendList and aniruddhaFriendList both point to the same memory loc

aniruddhaFriendList.append('Meghana')

print(ameyasFriendList)

# =============================================== #

def add(num1, num2):
    return num1 + num2

additionFn = add
# additionFn and add both point to the same memory location.


def getHelloMessage():
    return 'Hello'

def getGreetingMessage():
    return 'Welcome to this place'

def printMessage(someFn):
    message = someFn() # callback function
    print(message)

printMessage(getHelloMessage)
# pass the reference of getHelloMessage to printMessage

printMessage(getGreetingMessage)
# pass the reference of getGreetingMessage to printMessage