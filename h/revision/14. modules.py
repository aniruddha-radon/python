# what are modules
# modules are other (python) files which provide 
# functions or classes or attributes(variables)

# to import other modules =
# import <modulename>
import json
# import <modulename> as <alias>
import json as j
# from <modulename> import <something>
from json import loads, dumps
# from <modulename> import *, * = everything
from json import *

# custom module import
import variables
variables.x
variables.name

import variables as v
v.x
v.name
from variables import x, name
from variables import *
variables.name
variables.x