# print, len, input, type

# def cube(num):
#     return num ** 3

# cubeOfSix = cube(6)
# print(cubeOfSix)

# def add(num1, num2):
#     print(num1 + num2)

# # def  subtract(num1, num2):
# #     print(num1 - num2)

subtract = lambda n1, n2: n1 - n2

subtraction = subtract(12, 4)
print(subtraction)

# DRY => Dont Repeat Yourself
# If repeating => create function