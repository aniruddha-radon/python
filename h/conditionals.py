# assignment operator
numOne = 1

name = "Aniruddha"
nameTwo = "Ambareesh"
# comparison operator
isEqual = 9 == 9 # True
isEqual = "Heramb" == "Heramb" # True
isEqual = "Heramb" == "heramb" # False => case of H is different
isEqual = "Heramb" == name # False
isEqual = name == nameTwo # False
print(isEqual)

isNotEqual = 9 != 8 # True
isNotEqual = 9 != 9 # False
isNotEqual = "Heramb" != name # True
isNotEqual = name != nameTwo # True

# > Greater

isGreaterThan = 8 > 6 # True
isGreaterThan = 6 > 19 # False
isGreaterThan = 9 > 9 # False

# >= Greater Than Or Equal To
isGreaterThanEqualTo = 9 >= 9 # True

# <
isLessThan = 8 < 10 # True
isLessThan = 10 < 8 # False

# <=
isLessThanEqualTo = 8 <= 9 # True
isLessThanEqualTo = 8 <= 8 # True

# age > 21 to be able to serve liquor

age = input("Enter your age")
age = int(age)

isEligible = age > 21
if age > 25:
    print("You are eligible for ALL liquor")
elif age > 21: 
    print("You can get a beer")
else:
    print("You are NOT eligible for liquor")


userInput = input("Please enter a number")
userInput = int(userInput)

remainder = userInput % 3 

if remainder == 0:
    print("Tu laii bharri ahes")
else:
    print("Tu laii phaltu ahes")


dailyPracticeHours = [3, 4, 7, 9, 10, 12, 13, 8, 5, 4]
total = 3 + 4 + 7 + 9 + 10 + 12 + 13 + 8 + 5 + 4
average = total / len(dailyPracticeHours)

if average > 5:
    print("Awesome!")
else:
    print("ja baba!!!")