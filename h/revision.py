# general purpose programming language.
# computer related general tasks.
# console application
# web servers (interface)
# Machine Learning
# Data Analysis
# Data Science
# AI (generally nahi karat python madhna)

# variables
# help us to store/initialize values

# data types
# integer, float, string, list, tuple, dictionary, boolean (True/False), None
# var  assignment operator  value
number           =          10

# conditional (comparison) operator
# ==, >, >=, <, <=, !=
# LHS is compared with RHS and a boolean is evaluated 

# conditional statements
# if conditions:
#    if true execute this block
# elif conditions:
#    if true execute this block
# else:
#    otherwise execute this block

# loops
# while condition:
#      till condition is true keep executing this block
# 
# for => range(start, end)
# for every number in the range 'start' to 'end' keep exeuting
# 
# for elem in string/array/tuple/dictionary
#     str/array/tuple => every element is accessed through 'elem'
#     dictionary => every key is accessed 

# common function
# len, print, int, input, str, list, float, abs, index, split, random => choice, randint, sample

# creating function
# 
# def function_name(parameters):
#     function_body    

# def isOdd(num1):
#     oddAhe = num1 % 2 != 0
#     return oddAhe

# result = isOdd(5)
# print(result)