
# modes -> write, append, read, rw
# w => overwriting
# w+ => reading and overwriting
# a+ => reading and appending

with open('information.txt', 'a+') as info:
    info.seek(0)
    text = info.read()
    print(text)
    info.write('\nappended ss')