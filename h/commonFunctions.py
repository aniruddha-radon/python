# # print
# print(3)
# print("abcd")
# name = "Ambareesh"
# print(name)


# # input => user kadna kahitari data gheto
# age = input("Enter your age")

# # conversions
# number = int("8")
# string = str(8.14)

# # type 
# type(True) # => bool
# type(88) # => int
# type("wazzaaa") # => str
# name = "Gandu"
# type(name) # => str
# addition = 19
# type(addition) # => int

# # len => returns the length of string or array.
# name = "Aniruddha"
# characterCount = len(name)
# tempData = [14, 15, 13, 12, 16, 14, 13]
# numberOfDaysTempWasTaken = len(tempData)


# int, float, string, boolean, array/list, dictionary
exponent = pow(5, 8)
modulusValue = abs(-30) # => | -30 |
round(4.56) # 5
round(4.45) # 4
round(5.888888888889, 2) # 5.88


# string =>
message = "Hello Ambareesh!"
message[4] # o
message[:] # Hello Ambareesh!
message[3:] # lo Ambareesh!
message[:1] # H
message[1:5] # ello
message[-1] # !
print(message[1::2])
print(message[::-1])

userInput = '         Aniruddha   aaaa     '
print(userInput)
userInput = userInput.strip()
print(userInput)

message = 'i am angry!'
print(message.upper())

message = 'I AM SAD'
print(message.lower())

queryString = 'name=Aniruddha;id=12345;token=tu013'
print(queryString)
queryArray = queryString.split(';')
print(queryArray)