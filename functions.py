#print, input, range, len, round, max, min, abs, upper, lower, capitalize

def add(num1, num2):
    return num1 + num2

num1 = 1
num2 = 1

print(num1, num2)

for num in range(8):
    nextNumber = num1 + num2
    num1 = num2
    num2 = nextNumber
    print(nextNumber)

students = [
    ["Java", 50, 35, 76],
    ["Kargil", 76, 89, 87],
    ["Ramesh", 59, 70, 78]
] #2Dimensional

student1 = ["Aniruddha", 50, 35, 76]
student1[0]

students[0][0]

pixels = [
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 7, 8, 9]
]

pixels[0][7] = "Ambareesh"
print(pixels)