# print("what is your name?")

# # VARIABLE
# name = "Heramb"
# address = "Pune"
# age = 28

# print("my name is " + name)


# numberOne = 1 # integer
# numberTwo = 1.5 # decimal

# string = "A Double Quoted String"
# stringTwo = 'A Single quoted string'

# booleanOne = True
# booleanTwo = False

name = "Ambareesh" # 10000 -> name (Ambareesh)
print(name)

name = "Heramb"    # 10000 -> name (Heramb)
print(name)

name = "Aniruddha"
print(name)