# misc
# print("kahipan") # outputs to the terminal

# number = 10
# print(type(10))

# decimal = 19.5
# print(type(decimal))

# string = "abcd"
# print(type(string))

# boolean = True
# print(type(boolean))

n = 100000
for number in range(5, 10):
    print(number)

# age = input("Tuza vay kay?")
# age = int(age)
# print(f"Tuza vay ahe {age + 2}")

# NUMBERS ->

# print(pow(2, 5))
# print(abs(-70))
# print(round(78.99999, 2))
# print(bin(5))
# print(hex(16))

# string = "78"
# number = int(string)
# decimal = float(string)
# print(number, decimal)

# # STRINGS

# myIntro = "I'm Ambareesh, I live in Pune" 
# # print(myIntro)

# # print(myIntro[4])
# # print(myIntro[-4])
# # print(myIntro[:])
# # print(myIntro[2:])
# # print(myIntro[:2])
# # print(myIntro[2:9])

# # print(myIntro[::3])

# print(myIntro[::-1])

# print(myIntro.upper())
# print(myIntro.lower())


# naav = input("naav sang")
# naav = naav.strip()
# print(f"Tuza naav {naav} ahe")

# print(naav.count('a'))


# # LISTS

# myFriends = ['Heramb', 'Aniruddha', 'Ameya']
# numberOfFriends = len(myFriends)

# myFriends[1]
# myFriends[::-1]

# myFriends.append('Jay')



# print(myFriends)
# myFriends.pop()
# myFriends.pop()
# print(myFriends)

# myFriends.remove("Heramb")
# print(myFriends)


# percentagesOfStudents = [78, 34, 67, 65, 78, 67, 89, 88,87, 57, 34, 59, 96, 100, 23, 26, 62, 34]

# print(max(percentagesOfStudents))
# print(min(percentagesOfStudents))

dailyCountOfPasta = [12, 15,41, 67, 43, 32, 76, 100]
print(sum(dailyCountOfPasta))


